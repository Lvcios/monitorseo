﻿using SEOMonitor.Business;
using SEOMonitor.Model.Database.Monitor;
using SEOMonitor.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SEOMonitor.REST.Controllers
{
    [RoutePrefix("api/monitors")]
    public class GoogleResultsMonitorController : ApiController
    {
        private static string UrlGoogle = "https://www.google.com.mx/search?sclient=psy-ab&site=&source=hp&q={0}";
        private TextWriter textWriter;
        private GoogleResultBusiness googleResultBusiness = new GoogleResultBusiness(new CGoogleResultRepository());
        private GoogleTransactionBusiness googleTransactionBusiness = new GoogleTransactionBusiness(new CGoogleTransactionRepository());

        [Route("")]
        public IEnumerable<GoogleResultsMonitorModel> GetAllMonitors()
        {
            return googleResultBusiness.GetList();
        }

        [Route("{MonitorId}")]
        public GoogleResultsMonitorModel GetMonitor(long MonitorId)
        {
            return googleResultBusiness.GetById(MonitorId);
        }


    }
}
