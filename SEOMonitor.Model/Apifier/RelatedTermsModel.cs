﻿namespace SEOMonitor.Model.Apifier
{
    public class RelatedTermsModel
    {
        public string term { get; set; }
        public string link { get; set; }
    }
}