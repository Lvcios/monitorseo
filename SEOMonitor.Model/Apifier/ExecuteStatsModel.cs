﻿namespace SEOMonitor.Model.Apifier
{
    public class ExecuteStatsModel
    {
        public long? downloadedBytes { get; set; }
        public long? pagesInQueue { get; set; }
        public long? pagesCrawled { get; set; }
        public long? pagesOutputted { get; set; }
        public long? pagesFailed { get; set; }
        public long? pagesRetried { get; set; }
        public long? totalPageRetries { get; set; }
        public long? storageBytes { get; set; }
    }
}