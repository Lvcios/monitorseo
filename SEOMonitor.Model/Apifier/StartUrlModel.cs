﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Model.Apifier
{
    public class StartUrlModel
    {
        public string key { get; set; }
        public string value { get; set; }
    }
}
