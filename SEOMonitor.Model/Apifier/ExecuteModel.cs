﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Model.Apifier
{
    public class ExecuteModel
    {
        public string _id { get; set; }
        public string actId { get; set; }
        public DateTime startedAt { get; set; }
        public DateTime finishedAt { get; set; }
        public string status { get; set; }
        public string statusMessage { get; set; }
        public string detailsUrl { get; set; }
        public string resultsUrl { get; set; }
        public ExecuteStatsModel stats { get; set; }
    }
}
