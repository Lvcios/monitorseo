﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Model.Apifier
{
    public class ResultsModel
    {
        public string id { get; set; }
        public string url { get; set; }
        public string loadedUrl { get; set; }
        public DateTime requestedAt { get; set; }
        public DateTime loadingStartedAt { get; set; }
        public DateTime loadingFinishedAt { get; set; }
        public string loadErrorCode { get; set; }
        public DateTime pageFunctionStartedAt { get; set; }
        public DateTime pageFunctionFinishedAt { get; set; }
        public string uniqueKey { get; set; }
        public string type { get; set; }
        public bool isMainFrame { get; set; }
        public string postData { get; set; }
        public string contentType { get; set; }
        public string method { get; set; }
        public string willLoad { get; set; }
        public string label { get; set; }
        public string referrerId { get; set; }
        public long depth { get; set; }
        public string errorInfo { get; set; }
        public string interceptRequestData { get; set; }
        public long downloadedBytes { get; set; }
        public long storageBytes { get; set; }
        public GoogleResultsModel pageFunctionResult { get; set; }
        
    }
}
