﻿using System.Collections.Generic;

namespace SEOMonitor.Model.Apifier
{
    public class GoogleResultsModel
    {
        public string results { get; set; }
        public List<RelatedTermsModel> relatedTerms { get;set;}
    }
}