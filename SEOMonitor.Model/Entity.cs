﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Model
{
    public class Entity
    {
        [Column("id")]
        public long Id { get; set; }
    }
}
