﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Model.Database.Security
{
    [TableName("security.cat_user")]
    [PrimaryKey("id")]
    public class UserModel:Entity
    {
        [Column("vc_email")]
        public string Email { get; set; }
        [Column("vc_password")]
        public string Password { get; set; }
        [Column("tst_creationdate")]
        public DateTime CreationDate { get; set; }
        [Column("tst_lastlogin")]
        public DateTime LastLogin { get; set; }

        public UserModel() { }

    }
}
