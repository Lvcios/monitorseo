﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Model.Database.Transaction
{
    [TableName("transaction.tr_google_monitor")]
    [PrimaryKey("id")]
    public class GoogleMonitorTransactionModel:Entity
    {
        [Column("id_monitor")]
        public long MonitorId { get; set; }

        [Column("tst_transaction")]
        public DateTime Date { get; set; }

        [Column("vc_formattedresult")]
        public string FormattedResult { get; set; }

        [Column("vc_query")]
        public string Query { get; set; }

        [Column("vc_url")]
        public string Url { get; set; }

        [Column("n_results")]
        public long NumberOfResults { get; set; }
    
        [Column("n_timesearch")]
        public decimal TimeSearch { get; set; }


        public GoogleMonitorTransactionModel()
        {

        }

        public GoogleMonitorTransactionModel(long idMonitor, string formattedResult, string query, string url, long numberOfResults, decimal timeSearh)
        {
            this.MonitorId = idMonitor;
            this.FormattedResult = formattedResult;
            this.Query = query;
            this.Url = url;
            this.Date = DateTime.Now;
            this.NumberOfResults = numberOfResults;
            this.TimeSearch = timeSearh;

        }
    }
}
