﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEOMonitor.Model.Constants;

namespace SEOMonitor.Model.Database.Monitor
{
    [TableName("monitor.cat_google_monitor")]
    [PrimaryKey("id")]
    public class GoogleResultsMonitorModel:Entity
    {
        [Column("vc_monitor")]
        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Consulta")]
        [Column("vc_query")]
        public string Query { get; set; }

        [Display(Name = "URL")]
        [Column("vc_url")]
        public string Url { get; set; }

        [Display(Name = "Comentarios")]
        [Column("vc_comments")]
        public string Comments { get; set; }
        [Column("id_owner")]
        public long IdOwner { get; set; }

        [ResultColumn("vc_emailowner")]
        public string EmailOwner { get; set; }

        [Column("b_active")]
        [Display(Name = "Activo")]
        public bool Active { get; set; }

        [Display(Name = "Envio de emails")]
        [Column("b_sendmail")]
        public bool SendEmail { get; set; }

        [Column("id_status")]
        public int IdStatus { get; set; }

        public GoogleResultsMonitorModel() { }

        public GoogleResultsMonitorModel(string name, string query, string url, string comments, long idOwner, bool active, bool sendmail)
        {
            this.Name = name;
            this.Query = query;
            this.Url = url;
            this.Comments = comments;
            this.IdOwner = idOwner;
            this.Active = active;
            this.SendEmail = sendmail;
            this.IdStatus = 1;
        }

        public GoogleResultsMonitorModel(long id, string name, string query, string url, string comments, long idOwner, bool active, bool sendmail)
        {
            this.Id = id;
            this.Name = name;
            this.Query = query;
            this.Url = url;
            this.Comments = comments;
            this.IdOwner = idOwner;
            this.Active = active;
            this.SendEmail = sendmail;
            this.IdStatus = 1;
        }

        
    }
}
