﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Model.Database.Monitor
{
    [TableName("monitor.cat_relatedterm")]
    [PrimaryKey("id")]
    public class GoogleRelatedTermModel:Entity
    {
        [Column("vc_relatedterm")]
        public string RelatedTerm { get; set; }

        [Column("vc_url")]
        public string Url { get; set; }

        public GoogleRelatedTermModel()
        {

        }
    }
}
