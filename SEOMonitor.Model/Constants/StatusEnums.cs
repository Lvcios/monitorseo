﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Model.Constants
{
    public enum Status
    {
        MonitorDisponible = 1,
        MonitorEliminado = 2
    }
}
