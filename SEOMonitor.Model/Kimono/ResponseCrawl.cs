﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEOMonitor.Model.Kimono
{
    public class ResponseCrawl
    {
        public string name { get; set; }
        public int count { get; set; }
        public string frequency { get; set; }
        public int version { get; set; }
        public bool newdata { get; set; }
        public string lastrunstatus { get; set; }
        public string thisversionstatus { get; set; }
        public string thisversionrun { get; set; }
        public DynamicResults results { get; set; }
    }

    public class DynamicResults
    {
        public List<FormattedResults> Result { get; set; }
        public List<RelatedTerm> RelatedTerms { get; set; }
    }

    public class FormattedResults
    {
        public string FormattedResult { get; set; }
        public int index { get; set; }
        public string url { get; set; }
    }


    public class RelatedTerm
    {
        public Term Terms { get; set; }
        public int index { get; set; }
        public string url { get; set; }
    }

    public class Term
    {
        public string href { get; set; }
        public string text { get; set; }
    }
}