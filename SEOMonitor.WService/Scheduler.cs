﻿using Inhouse.Sdk.Logger;
using RestSharp;
using SEMonitor.Core;
using SEOMonitor.Business;
using SEOMonitor.Core;
using SEOMonitor.Model.Apifier;
using SEOMonitor.Model.Database.Monitor;
using SEOMonitor.Model.Database.Transaction;
using SEOMonitor.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Web.Script.Serialization;

namespace SEOMonitor.WService
{
    public partial class Scheduler : ServiceBase
    {
        private System.Timers.Timer timer = null;
        private System.Timers.Timer timerComparer = null;
        private GoogleTransactionBusiness googleTransactionBusiness = new GoogleTransactionBusiness(new CGoogleTransactionRepository());
        private GoogleResultBusiness googleResultBusiness = new GoogleResultBusiness(new CGoogleResultRepository());
        private GoogleRelatedTermBusiness googleRelatedTermBusiness = new GoogleRelatedTermBusiness(new CGoogleRelatedTermRepository());
        public Scheduler()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            timer = new System.Timers.Timer();
            timerComparer = new System.Timers.Timer();

            this.timerComparer.Interval = 10000;
            this.timerComparer.Elapsed += new System.Timers.ElapsedEventHandler(this.Compare);
            this.timerComparer.Enabled = true;

            //this.timer.Interval = 1000; 
            this.timer.Interval = int.Parse(ConfigurationManager.AppSettings["IntervalMinutes"]) * 60 * 1000;
            this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer_Tick);
            //this.timer.Enabled = true;
            //LogProviderManager.Logger.Log(LogType.debug, "Start Service");
            //LogProviderManager.Logger.Log(LogType.debug, string.Format("El servició se ejecutara cada {0} segundos", (this.timer.Interval / 1000)));
        }

        private void Compare(object sender, ElapsedEventArgs e)
        {
            LogProviderManager.Logger.Log(LogType.debug, "Ajustando timer");
            if (DateTime.Now.Minute%2 == 0)
            {
                this.timerComparer.Stop();
                LogProviderManager.Logger.Log(LogType.debug, "Timer Ajustado");
                LogProviderManager.Logger.Log(LogType.debug, "Ejecuta evento incial");
                LogProviderManager.Logger.Log(LogType.debug, "El próximo evento será en " + DateTime.Now.AddMilliseconds(this.timer.Interval).ToString());
                this.Execute();
                this.timer.Start();
            }
        }

        protected override void OnStop()
        {
            timer.Enabled = false;
            LogProviderManager.Logger.Log(LogType.debug, "Stop Service");
        }

        public void timer_Tick(object sender, ElapsedEventArgs e)
        {
            this.Execute();
        }

        /// <summary>
        /// Se ejecuta cada n segundos según el intervalo indicado
        /// </summary>
        public void Execute()
        {
            List<GoogleResultsMonitorModel> monitorList = new List<GoogleResultsMonitorModel>();
            try
            {
                //monitorList = GoogleResultsMonitor.Get().Where(m => m.Active == true).ToList();
                monitorList = googleResultBusiness.GetList();
                LogProviderManager.Logger.Log(LogType.debug, "Total de monitores por realizar transacción: " + monitorList.Count.ToString());
                //return;
            }
            catch(Exception ex)
            {
                LogProviderManager.Logger.Log(LogType.debug, ex.Message);
            }

            try
            {
                foreach (GoogleResultsMonitorModel monitor in monitorList)
                {
                    LogProviderManager.Logger.Log(LogType.info, "Starting transaction");
                    LogProviderManager.Logger.Log(LogType.info, string.Format("Monitor: {0}", monitor.Name));

                    ApifierCrawler crawler = new ApifierCrawler();
                    ResultsModel results = crawler.ExecuteCrawl(monitor.IdOwner, monitor);

                    LogProviderManager.Logger.Log(LogType.info, new JavaScriptSerializer().Serialize(results));

                    string formattedresult = "";
                    if (results.pageFunctionResult != null)
                    {

                        long numberOfResults = 0;
                        decimal timeSearch = 0;
                        Tuple<long, decimal> resultsTuple = new Tuple<long, decimal>(numberOfResults, timeSearch);
                        resultsTuple = Core.Utils.GetNumberOfResultsAndTime(results.pageFunctionResult.results);

                        GoogleMonitorTransactionModel transaction = new GoogleMonitorTransactionModel(monitor.Id, formattedresult, monitor.Query, monitor.Url, numberOfResults, timeSearch);
                        //transaction.Save();
                        googleTransactionBusiness.Save(transaction);
                        string relatedTerms = "";
                        if (results.pageFunctionResult.relatedTerms != null)
                        {
                            foreach (var item in results.pageFunctionResult.relatedTerms)
                            {
                                GoogleRelatedTermModel newTerm = new GoogleRelatedTermModel();
                                newTerm.RelatedTerm = item.term;
                                newTerm.Url = item.link;
                                googleRelatedTermBusiness.Save(newTerm, transaction.Id);
                                relatedTerms += string.Format("{0},", newTerm.RelatedTerm);
                            }
                            relatedTerms = relatedTerms.Substring(0, relatedTerms.Length - 1);
                        }
                        //Envia Email de reporte sobre la transacción
                        try
                        {
                            if (monitor.SendEmail)
                            {
                                //Mail mail = new Mail(model.EmailOwner, "User", "Reporte de búsqueda");
                                Mail mail = new Mail("lucio@gimoti.com", "User", "Reporte de búsqueda");
                                StringBuilder sb = new StringBuilder();
                                sb.Append(string.Format("<h2>Resultados del monitor \"{0}\"</h2><br/>", monitor.Name));
                                sb.Append(string.Format("<p>Url: <a href='{0}'>{0} link de descarga</a></<p>", transaction.Url));
                                sb.Append(string.Format("<p>Respuesta: {0}</<p>", transaction.FormattedResult));
                                sb.Append(string.Format("<p>Terminos relacionados: {0}</<p>", relatedTerms));
                                sb.Append(string.Format("<p>Fecha y hora {0}</<p>", transaction.Date.ToString()));
                                mail.SendReportEmail(sb.ToString());
                            }

                        }
                        catch (Exception ex)
                        {
                            LogProviderManager.Logger.Log(LogType.debug, ex.Message);
                        }
                        LogProviderManager.Logger.Log(LogType.info, "Transaction Done");
                    }
                }
            }
            catch(Exception ex)
            {
                LogProviderManager.Logger.Log(LogType.debug, ex.Message);
            }
            
            LogProviderManager.Logger.Log(LogType.debug, "El próximo evento será en " + DateTime.Now.AddMilliseconds(this.timer.Interval).ToString());
        }
    }
}
