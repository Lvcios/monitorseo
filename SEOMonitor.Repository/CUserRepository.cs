﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEOMonitor.Model.Database.Security;

namespace SEOMonitor.Repository
{
    public class CUserRepository : IUserRepository
    {
        public bool Authenticate(string Email, string Password)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<bool>("SELECT * FROM security.fn_get_authenticate(@0,@1)", Email, Password).FirstOrDefault();
            }
        }

        public void Delete(long Id)
        {
            throw new NotImplementedException();
        }

        public UserModel GetByEmail(string Email)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<UserModel>("SELECT * FROM security.fn_get_userbyemail(@0)", Email).FirstOrDefault();
            }
        }

        public UserModel GetById(long Id)
        {
            throw new NotImplementedException();
        }

        public List<UserModel> GetList()
        {
            throw new NotImplementedException();
        }

        public bool Register(string Email, string Password)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<bool>("SELECT * FROM security.fnw_register_user(@0,@1)", Email, Password).FirstOrDefault();
            }
        }

        public void Save(UserModel entity)
        {
            throw new NotImplementedException();
        }

        public void Update(UserModel entity)
        {
            throw new NotImplementedException();
        }

        public void UpdatePassword(string Email, string Password)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                db.Execute("SELECT * FROM security.fnw_update_password(@0,@1)", Email, Password);
            }
        }
    }
}
