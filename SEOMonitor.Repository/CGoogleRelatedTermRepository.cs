﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEOMonitor.Model;
using SEOMonitor.Model.Database.Monitor;

namespace SEOMonitor.Repository
{
    public class CGoogleRelatedTermRepository : IGoogleRelatedTermRepository
    {
        public void Delete(long Id)
        {
            throw new NotImplementedException();
        }

        public GoogleRelatedTermModel GetById(long Id)
        {
            throw new NotImplementedException();
        }

        public List<GoogleRelatedTermModel> GetList()
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<GoogleRelatedTermModel>("").ToList();
            }
        }

        public List<GoogleRelatedTermModel> GetRelatedTermsByTransaction(long TransactionId)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<GoogleRelatedTermModel>("SELECT * FROM monitor.fn_get_relatedtermsbytransaction(@0);", TransactionId).ToList();
            }
        }

        public void Save(GoogleRelatedTermModel entity)
        {
            using(PetaPoco.Database db =  new PetaPoco.Database("SEOMonitor"))
            {
                db.Save(entity);
            }
        }

        public void Save(GoogleRelatedTermModel entity, long TransactionId)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                entity.Id = db.Query<int>("SELECT * FROM transaction.fnw_save_relatedterm(@0,@1,@2)", entity.RelatedTerm, entity.Url, TransactionId).FirstOrDefault();
            }
        }

        public void Update(GoogleRelatedTermModel entity)
        {
            throw new NotImplementedException();
        }
    }
}
