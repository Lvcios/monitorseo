﻿using SEOMonitor.Model.Database.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Repository
{
    public interface IUserRepository: IRepository<UserModel>
    {
        void UpdatePassword(string Email, string Password);
        bool Authenticate(string Email, string Password);
        UserModel GetByEmail(string Email);
        bool Register(string Email, string Password);
    }
}
