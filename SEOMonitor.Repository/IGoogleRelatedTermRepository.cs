﻿using SEOMonitor.Model;
using SEOMonitor.Model.Database.Monitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Repository
{
    public interface IGoogleRelatedTermRepository:IRepository<GoogleRelatedTermModel>
    {
        List<GoogleRelatedTermModel> GetRelatedTermsByTransaction(long TransactionId);
        void Save(GoogleRelatedTermModel entity, long TransactionId);
    }
}
