﻿using SEOMonitor.Model.Database.Monitor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Repository
{
    public interface IGoogleResultRespository:IRepository<GoogleResultsMonitorModel>
    {
        List<GoogleResultsMonitorModel> GetByIdOwner(long OwnerId);
        GoogleResultsMonitorModel GetByIdOwner(long OwnerId, long MonitorId);
    }
}
