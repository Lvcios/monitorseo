﻿using SEOMonitor.Model.Database.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Repository
{
    public interface IGoogleTransactionRepository:IRepository<GoogleMonitorTransactionModel>
    {
        List<GoogleMonitorTransactionModel> GetList(long MonitorId, DateTime? startDate, DateTime? endDate);
        List<GoogleMonitorTransactionModel> GetList(long MonitorId);
        List<GoogleMonitorTransactionModel> GetList(long Id, long MonitorId);
    }
}
