﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEOMonitor.Model.Database.Transaction;

namespace SEOMonitor.Repository
{
    public class CGoogleTransactionRepository : IGoogleTransactionRepository
    {
        public void Delete(long Id)
        {
            throw new NotImplementedException();
        }

        public GoogleMonitorTransactionModel GetById(long Id)
        {
            throw new NotImplementedException();
        }

        public List<GoogleMonitorTransactionModel> GetList()
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<GoogleMonitorTransactionModel>("").ToList();
            }
        }

        public List<GoogleMonitorTransactionModel> GetList(long MonitorId)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<GoogleMonitorTransactionModel>("WHERE id_monitor = @0 AND tst_transaction >= @1;", MonitorId, DateTime.Now.AddHours(-24).Date).ToList();
            }
        }

        public List<GoogleMonitorTransactionModel> GetList(long Id, long MonitorId)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<GoogleMonitorTransactionModel>("WHERE id_transaction = @0 AND id_monitor = @1", Id, MonitorId).ToList();
            }
        }

        public List<GoogleMonitorTransactionModel> GetList(long MonitorId, DateTime? startDate, DateTime? endDate)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<GoogleMonitorTransactionModel>("WHERE id_monitor = @0 AND tst_transaction BETWEEN @1 AND @2;", MonitorId, startDate, endDate).ToList();
            }
        }

        public void Save(GoogleMonitorTransactionModel entity)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                db.Save(entity);
            }
        }

        public void Update(GoogleMonitorTransactionModel entity)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                db.Update(entity);
            }
        }
    }
}
