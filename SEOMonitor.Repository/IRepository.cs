﻿using SEOMonitor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Repository
{
    public interface IRepository<T> where T : Entity
    {
        List<T> GetList();
        void Save(T entity);
        void Update(T entity);
        void Delete(long Id);
        T GetById(long Id);
    }
}
