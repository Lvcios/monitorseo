﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEOMonitor.Model.Database.Monitor;
using SEOMonitor.Model.Constants;

namespace SEOMonitor.Repository
{
    public class CGoogleResultRepository : IGoogleResultRespository
    {
        public void Delete(long Id)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                var itemToDelete = db.Query<GoogleResultsMonitorModel>("WHERE id = @0", Id).FirstOrDefault();
                if(itemToDelete != null)
                {
                    itemToDelete.IdStatus = (int)Status.MonitorEliminado;
                    db.Update(itemToDelete);
                }
            }
        }

        public GoogleResultsMonitorModel GetById(long Id)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<GoogleResultsMonitorModel>("WHERE id = @0 AND id_status = @1", Id, (int)Status.MonitorDisponible).FirstOrDefault();
            }
        }

        public List<GoogleResultsMonitorModel> GetByIdOwner(long OwnerId)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<GoogleResultsMonitorModel>("WHERE id_owner = @0 AND id_status = @1", OwnerId, (int)Status.MonitorDisponible).ToList();
            }
        }

        public GoogleResultsMonitorModel GetByIdOwner(long MonitorId, long OwnerId)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<GoogleResultsMonitorModel>("WHERE id = @0 AND id_owner = @1 AND id_status = @2", MonitorId, OwnerId, (int)Status.MonitorDisponible).FirstOrDefault();
            }
        }

        public List<GoogleResultsMonitorModel> GetList()
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                return db.Query<GoogleResultsMonitorModel>("SELECT * FROM monitor.fn_get_googlemonitors()").ToList();
            }
        }

        public void Save(GoogleResultsMonitorModel entity)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                db.Save(entity);
            }
        }

        public void Update(GoogleResultsMonitorModel entity)
        {
            using (PetaPoco.Database db = new PetaPoco.Database("SEOMonitor"))
            {
                db.Update(entity);
            }
        }
    }
}
