﻿using Newtonsoft.Json;
using RestSharp;
using SEOMonitor.Model.Apifier;
using SEOMonitor.Model.Database.Monitor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SEMonitor.Core
{
    public class ApifierCrawler
    {
        private string ApiId { get; set; }
        private string TokenId { get; set; }
        private string ClientUrl { get; set; }
        private string ExecuteCrawlUrl { get; set; }

        public ApifierCrawler()
        {
            this.ApiId = ConfigurationManager.AppSettings["ApifierApiId"];
            this.TokenId = ConfigurationManager.AppSettings["ApifierApiToken"];
            this.ClientUrl = ConfigurationManager.AppSettings["ApifierClient"];
        }

        public ResultsModel ExecuteCrawl(long UserId, GoogleResultsMonitorModel monitor)
        {
            var client = new RestClient(this.ClientUrl);

            var request = new RestRequest(string.Format("v1/{0}/crawlers/GoogleSearch/execute?token={1}",
                this.ApiId,
                this.TokenId), Method.POST);

            StartUrlModel[] array = new StartUrlModel[1];
            array[0] = new StartUrlModel
            {
                key = "searchTerm",
                value = string.Format("https://www.google.com.mx/search?sclient=psy-ab&site=&source=hp&q={0}", monitor.Query)
            };

            request.AddJsonBody(
                new
                {
                    startUrls = array
                });

            request.AddHeader("Content-Type", "application/json");
            var response = client.Execute(request);
            ExecuteModel crawl = JsonConvert.DeserializeObject<ExecuteModel>(response.Content, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //v1/execs/vFLOCuOzRsWJ8GeAC/results
            Thread.Sleep(10000);
            request = new RestRequest(string.Format("/v1/execs/{0}/results", crawl._id), Method.GET);
            response = client.Execute(request);
            List<ResultsModel> results = JsonConvert.DeserializeObject<List<ResultsModel>>(response.Content, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            if(results != null)
            {
                return results[0];
            }
            else
            {
                return new ResultsModel();
            }
            
        }
    }
}
