﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Core
{
    public class Mail
    {
        public string UserNameFrom { get; set; }
        public string EmailFrom { get; set; }
        public string UserNameTo { get; set; }
        public string EmailTo { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool UseSSL { get; set; }
        public bool IsHtml { get; set; }

        public Mail(string _emailTo, string _usernameto, string _subject)
        {
            this.EmailFrom = "no-reply@gimoti.com";
            this.UserNameFrom = "Gimoti. Emociones de lealtad.";
            this.EmailTo = _emailTo;
            this.UserNameTo = _usernameto;
            this.Subject = _subject;
        }

        private string PopulateBody(string userName, string description)
        {
            //String name = new CultureInfo("es-MX", true).TextInfo.ToTitleCase(userName.ToLower());
            string body = string.Empty;
            string path = AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplates/Base.html";
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{UserName}", userName.ToUpper());
            body = body.Replace("{Description}", description);
            body = body.Replace("{Dear}", "Estimado");
            body = body.Replace("{Gimoti}", "Monitor SEO");
            body = body.Replace("{EmailContac}", "contact@gimoti.com");
            return body;
        }

        public void SendReportEmail(string body)
        {
            this.Body = PopulateBody("Usuario", body);
            SendEmail();
        }

        private void SendEmail()
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                var credentialUserName = "lucio@gimoti.com";
                var pwd = "marketingdigital2015";
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("mail.gimoti.com");
                client.Port = 25;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(credentialUserName, pwd);
                client.EnableSsl = false;
                client.Credentials = credentials;
                var mail = new System.Net.Mail.MailMessage(credentialUserName, this.EmailTo);
                mail.Subject = this.Subject;
                mail.Body = this.Body;
                mail.IsBodyHtml = true;
                mail.Sender = new System.Net.Mail.MailAddress(credentialUserName);
                mail.From = new System.Net.Mail.MailAddress(this.EmailFrom, "Gimoti");
                client.Send(mail);
            }
        }
    }
}
