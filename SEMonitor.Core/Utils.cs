﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Core
{
    public class Utils
    {
        public static Tuple<long, decimal> GetNumberOfResultsAndTime(string formattedResults)
        {

            string decimalNumber = "";
            string timeResults = "";

            for (int i = 0; i < formattedResults.Length; i++)
            {
                if (Char.IsNumber(formattedResults[i]))
                {
                    for(int j = i; j < formattedResults.Length; j++)
                    {
                        if (!Char.IsSeparator(formattedResults[j]))
                        {
                            decimalNumber += formattedResults[j];
                        }
                        else{

                            string[] r1 = formattedResults.Split('(');
                            string[] r2 = r1[1].Split(')');
                            string[] r3 = r2[0].Split(' ');
                            timeResults = r3[0];
                            break;
                        }
                    }
                    break;
                }
            }

            decimalNumber = decimalNumber.Replace(",", string.Empty);
            long numberOfResults = 0;
            decimal timeSearch = 0;
            long.TryParse(decimalNumber, out numberOfResults);
            decimal.TryParse(timeResults, out timeSearch);

            return new Tuple<long, decimal>(numberOfResults, timeSearch);
        }
    }
}
