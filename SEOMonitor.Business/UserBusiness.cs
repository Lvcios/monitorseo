﻿using SEOMonitor.Model.Database.Security;
using SEOMonitor.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Business
{
    public class UserBusiness
    {
        IUserRepository _repository;

        public UserBusiness(IUserRepository repository)
        {
            this._repository = repository;
        }

        public List<UserModel> GetUsers()
        {
            return _repository.GetList();
        }

        public bool Authenticate(string Email, string Password)
        {
            return _repository.Authenticate(Email, Password);
        }

        public UserModel GetByEmail(string Email)
        {
            return _repository.GetByEmail(Email);
        }

        public void UpdatePassword(string Email, string NewPassword)
        {
            _repository.UpdatePassword(Email, NewPassword);
        }

        public bool Register(string Email, string Password)
        {
            return _repository.Register(Email, Password);
        }
    }
}
