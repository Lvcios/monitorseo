﻿using SEOMonitor.Model.Database.Monitor;
using SEOMonitor.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEOMonitor.Business
{
    public class GoogleResultBusiness
    {
        IGoogleResultRespository _repository;

        public GoogleResultBusiness(IGoogleResultRespository repository)
        {
            this._repository = repository;
        }

        public List<GoogleResultsMonitorModel> GetByOwner(long OwnerId)
        {
            return _repository.GetByIdOwner(OwnerId);
        }

        public void Update(GoogleResultsMonitorModel entity)
        {
            _repository.Update(entity);
        }

        public GoogleResultsMonitorModel GetByOwner(long MonitorId, long OwnerId)
        {
            return _repository.GetByIdOwner(MonitorId, OwnerId);
        }

        public void Save(GoogleResultsMonitorModel monitor)
        {
            _repository.Save(monitor);
        }

        public void Delete(long Id)
        {
            _repository.Delete(Id);
        }

        public GoogleResultsMonitorModel GetById(long monitorId)
        {
            return _repository.GetById(monitorId);
        }

        public List<GoogleResultsMonitorModel> GetList()
        {
            return _repository.GetList();
        }
    }
}
