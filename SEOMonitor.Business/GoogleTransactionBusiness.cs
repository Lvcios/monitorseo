﻿using SEOMonitor.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEOMonitor.Model.Database.Transaction;

namespace SEOMonitor.Business
{
    public class GoogleTransactionBusiness
    {
        IGoogleTransactionRepository _repository;

        public GoogleTransactionBusiness(IGoogleTransactionRepository repository)
        {
            this._repository = repository;
        }

        public List<GoogleMonitorTransactionModel> GetList(long MonitorId)
        {
            return _repository.GetList(MonitorId);
        }

        public List<GoogleMonitorTransactionModel> GetList(long MonitorId, DateTime? startDate, DateTime? endDate)
        {
            return _repository.GetList(MonitorId, startDate, endDate);
        }

        public void Save(GoogleMonitorTransactionModel transaction)
        {
            _repository.Save(transaction);
        }
    }
}
