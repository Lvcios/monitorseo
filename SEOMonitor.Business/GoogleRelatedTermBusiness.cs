﻿using SEOMonitor.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEOMonitor.Model.Database.Monitor;

namespace SEOMonitor.Business
{
    public class GoogleRelatedTermBusiness
    {
        IGoogleRelatedTermRepository _repository;

        public GoogleRelatedTermBusiness(CGoogleRelatedTermRepository repository)
        {
            this._repository = repository;
        }

        public void Save(GoogleRelatedTermModel newTerm, long TransactionId)
        {
            _repository.Save(newTerm, TransactionId);
        }

        public List<GoogleRelatedTermModel> GetRelatedTermsByTransaction(long TransactionId)
        {
            return _repository.GetRelatedTermsByTransaction(TransactionId);
        }
    }
}
