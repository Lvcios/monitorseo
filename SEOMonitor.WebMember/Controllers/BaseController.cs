﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace SEOMonitor.WebMember.Controllers
{
    
    public class BaseController : Controller
    {
        public static Model.Database.Security.UserModel GetCurrentUser(HttpRequestBase Request)
        {
            var cookie = Request.Cookies["UserSEOMonitor"];
            if(cookie == null)
            {
                return null;
            }
            
            var cookieContent = FormsAuthentication.Decrypt(cookie.Value);
            var user = new JavaScriptSerializer().Deserialize<Model.Database.Security.UserModel>(cookieContent.UserData);
            return user;
        }


        

    }
}