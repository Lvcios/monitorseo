﻿using SEOMonitor.Business;
using SEOMonitor.Repository;
using SEOMonitor.WebMember.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace SEOMonitor.WebMember.Controllers
{
    public class AccountController : Controller
    {
        private UserBusiness userBusiness = new UserBusiness(new CUserRepository());
        // GET: Account
        public ActionResult Index()
        {

            if(BaseController.GetCurrentUser(Request) != null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [Authorize]
        public ActionResult MyAccount()
        {

            var user = BaseController.GetCurrentUser(Request);
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult MyAccount(ChangePasswordModel model)
        {
            ModelState.Remove("Email");
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = BaseController.GetCurrentUser(Request);
            userBusiness.UpdatePassword(user.Email, model.NewPassword);
            //user.Password = model.NewPassword;
            //user.UpdatePassword();
            
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            bool authenticate = userBusiness.Authenticate(model.Email, model.Password);

            if (!authenticate)
            {
                return View(model);
            }

            var user = userBusiness.GetByEmail(model.Email);

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, "UserSEOMonitor", DateTime.Now, DateTime.Now.AddMinutes(30), true, new JavaScriptSerializer().Serialize(user));
            string cookieContent = FormsAuthentication.Encrypt(ticket);

            HttpCookie cookie = new HttpCookie("UserSEOMonitor", cookieContent);
            Response.Cookies.Add(cookie);
            FormsAuthentication.SetAuthCookie(model.Email, true);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            bool registered = userBusiness.Register(model.Email, model.Password);

            if (!registered)
            {
                return View(model);
            }

            return RedirectToAction("Login", "Account");
        }
        
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            System.Web.HttpContext.Current.Session.Clear();
            return RedirectToAction("Index");
        }
    }
}