﻿using RestSharp;
using SEMonitor.Core;
using SEOMonitor.Business;
using SEOMonitor.Model.Apifier;
using SEOMonitor.Model.Database.Monitor;
using SEOMonitor.Model.Database.Transaction;
using SEOMonitor.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SEOMonitor.WebMember.Controllers
{
    public class CrawlerController : Controller
    {
        private GoogleResultBusiness googleResultBusiness = new GoogleResultBusiness(new CGoogleResultRepository());
        private GoogleTransactionBusiness googleTransactionBusiness = new GoogleTransactionBusiness(new CGoogleTransactionRepository());
        private GoogleRelatedTermBusiness googleRelatedTermBusiness = new GoogleRelatedTermBusiness(new CGoogleRelatedTermRepository());
        public ActionResult ExecuteSearch(long MonitorId)
        {
            var currentUser = BaseController.GetCurrentUser(Request);
            GoogleResultsMonitorModel monitor = googleResultBusiness.GetByOwner(MonitorId, currentUser.Id);
            ApifierCrawler crawler = new ApifierCrawler();
            ResultsModel results = crawler.ExecuteCrawl(currentUser.Id, monitor);

            string formattedresult = "";
            if (results.pageFunctionResult != null)
            {
                long numberOfResults = 0;
                decimal timeSearch = 0;
                Tuple<long, decimal> resultsTuple = new Tuple<long, decimal>(numberOfResults, timeSearch);
                resultsTuple = Core.Utils.GetNumberOfResultsAndTime(results.pageFunctionResult.results);
                formattedresult = results.pageFunctionResult.results;
                GoogleMonitorTransactionModel transaction = new GoogleMonitorTransactionModel(
                    monitor.Id, formattedresult, monitor.Query, 
                    monitor.Url, resultsTuple.Item1, resultsTuple.Item2);
                googleTransactionBusiness.Save(transaction);
                string relatedTerms = "";
                if (results.pageFunctionResult.relatedTerms != null)
                {
                    foreach (var item in results.pageFunctionResult.relatedTerms)
                    {
                        GoogleRelatedTermModel newTerm = new GoogleRelatedTermModel();
                        newTerm.RelatedTerm = item.term;
                        newTerm.Url = item.link;
                        googleRelatedTermBusiness.Save(newTerm, transaction.Id);
                        relatedTerms += string.Format("{0},", newTerm.RelatedTerm);
                    }
                    relatedTerms = relatedTerms.Substring(0, relatedTerms.Length - 1);
                }
            }
            //return Content(new JavaScriptSerializer().Serialize(results));
            return RedirectToAction("Details", "Home", new { Id = MonitorId });
        }
    }
}