﻿using RestSharp;
using SEOMonitor.Model.Database.Monitor;
using SEOMonitor.Model.Database.Transaction;
using SEOMonitor.Model.Kimono;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SEOMonitor.WebMember.Controllers
{
    public class KimonoController : Controller
    {
        /// <summary>
        /// Ejecuta una petición de Crawl a la api de kimono
        /// </summary>
        /// <param name="id_monitor">id del monitor</param>
        /// <returns>retorna un objeto con el status de la petición</returns>
        public ActionResult ExcuteSearch(int id_monitor)
        {
            var currentUser = BaseController.GetCurrentUser(Request);
            GoogleResultsMonitor model = GoogleResultsMonitor.Get(id_monitor, currentUser.Id);
            
            var client = new RestClient("https://www.kimonolabs.com/");
            //var request = new RestRequest(string.Format("kimonoapis/{0}/update", "8op3l144"), Method.POST);//api pablo
            var request = new RestRequest(string.Format("kimonoapis/{0}/update", "8op3l144"), Method.POST);
            request.AddJsonBody(
                new
                {
                    //apikey = "Gv7EwK0pu87hcLDgRDOubFFumfou50yt",
                    apikey = "Gv7EwK0pu87hcLDgRDOubFFumfou50yt",
                    targeturl = model.Url
                });
            request.AddHeader("Content-Type", "application/json");
            var response = client.Execute(request);
            var content = response.Content;

            //realiza una petición a la api
            request = new RestRequest(string.Format("kimonoapis/{0}/startcrawl", "8op3l144"), Method.POST);
            //request.AddJsonBody(new { apikey = "rBiqKLebYO7HmxqjBH8uMgzJ8jzaWBh6" });
            request.AddJsonBody(new { apikey = "Gv7EwK0pu87hcLDgRDOubFFumfou50yt" });
            response = client.Execute(request);
            content = response.Content;

            //pausa de 10 segundos para realizar el scrapping
            Thread.Sleep(30000);


            //obtiene los resutaldos
            //request = new RestRequest(string.Format("/api/{0}?apikey={1}", "8op3l144", "rBiqKLebYO7HmxqjBH8uMgzJ8jzaWBh6"), Method.GET);
            request = new RestRequest(string.Format("/api/{0}?apikey={1}", "8op3l144", "Gv7EwK0pu87hcLDgRDOubFFumfou50yt"), Method.GET);
            response = client.Execute(request);
            content = response.Content;
            ResponseCrawl crawl = new JavaScriptSerializer().Deserialize<ResponseCrawl>(content);

            string formattedresult = "";
            if(crawl.results != null)
            {

                foreach (var item in crawl.results.Result)
                {
                    formattedresult = item.FormattedResult;
                    break;
                }

                decimal numberOfResults = 0;
                decimal timeSearch = 0;
                Tuple<decimal, decimal> results = new Tuple<decimal, decimal>(numberOfResults, timeSearch);
                if(formattedresult != "")
                {
                    results = Core.Utils.GetNumberOfResultsAndTime(formattedresult);
                    numberOfResults = results.Item1;
                    timeSearch = results.Item2;
                }

                GoogleMonitorTransaction transaction = new GoogleMonitorTransaction(model.Id, formattedresult, model.Query, model.Url, numberOfResults, timeSearch);
                transaction.Save();

                if(crawl.results.RelatedTerms != null)
                {
                    foreach (var item in crawl.results.RelatedTerms)
                    {
                        GoogleRelatedTerm newTerm = new GoogleRelatedTerm();
                        newTerm.RelatedTerm = item.Terms.text;
                        newTerm.Url = item.Terms.href;
                        newTerm.Save(transaction.Id);
                    }
                }
            }

            return RedirectToAction("Details", "Home", new { Id = model.Id });
        }

        public ActionResult Api()
        {
            var client = new RestClient("https://www.kimonolabs.com/");
            var request = new RestRequest(string.Format("/api/{0}?apikey={1}", "8op3l144", "Gv7EwK0pu87hcLDgRDOubFFumfou50yt"), Method.GET);
            var response = client.Execute(request);
            var content = response.Content;
            ResponseCrawl crawl = new JavaScriptSerializer().Deserialize<ResponseCrawl>(content);
            string formattedresult = "";
            foreach (var item in crawl.results.Result)
            {
                formattedresult = item.FormattedResult;
                break;
            }
            //revisar si el término está en el catálogo, 
            //si no está, agregarlo al catalogo y hacer insert en la tabla de relacion de transacciones con terminos de búsqueda
            // si esta, solo hacer la inserción a la tabla de relación de transacciones
            return Json(crawl, JsonRequestBehavior.AllowGet);
        }
    }
}