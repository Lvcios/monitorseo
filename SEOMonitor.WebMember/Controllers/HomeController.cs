﻿using CsvHelper;
using CsvHelper.Configuration;
using Inhouse.Sdk.Logger;
using SEOMonitor.Model.Database.Monitor;
using SEOMonitor.Model.Database.Transaction;
using SEOMonitor.WebMember.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using SEOMonitor.Business;
using SEOMonitor.Repository;

namespace SEOMonitor.WebMember.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        private static string UrlGoogle = "https://www.google.com.mx/search?sclient=psy-ab&site=&source=hp&q={0}";
        private TextWriter textWriter;
        private GoogleResultBusiness googleResultBusiness = new GoogleResultBusiness(new CGoogleResultRepository());
        private GoogleTransactionBusiness googleTransactionBusiness = new GoogleTransactionBusiness(new CGoogleTransactionRepository());

        // GET: Home
        public ActionResult Index()
        {
            var currentUser = BaseController.GetCurrentUser(Request);
            if (currentUser == null)
            {
                return RedirectToAction("LogOut", "Account");
            }
            List<GoogleResultsMonitorModel> list = googleResultBusiness.GetByOwner(currentUser.Id);
            //GoogleResultsMonitor.Get(currentUser.Id).OrderBy(m => m.Name).ToList();

            //List<GoogleMonitorTransaction> toedit = GoogleMonitorTransaction.Get();
            //foreach(var tr in toedit)
            //{
            //    var tuple = Core.Utils.GetNumberOfResultsAndTime(tr.FormattedResult);
            //    tr.NumberOfResults = tuple.Item1;
            //    tr.TimeSearch = tuple.Item2;
            //    tr.Update();
            //}

            return View(list);
        }

        public ActionResult Create()
        {
            RegisterMonitorModel model = new RegisterMonitorModel();
            model.Active = true;
            model.SendEmail = true;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(RegisterMonitorModel model)
        {
            ModelState.Remove("Id");
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var currentUser = BaseController.GetCurrentUser(Request);

            GoogleResultsMonitorModel monitor = new GoogleResultsMonitorModel(model.Name,
                model.Query, 
                string.Format(UrlGoogle, Server.HtmlEncode(model.Query)),
                model.Comments,
                currentUser.Id,
                model.Active,
                model.SendEmail
                );

            //monitor.Save();
            googleResultBusiness.Save(monitor);

            if(monitor.Id > 0)
            {
                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Edit(int Id)
        {
            var currentUser = BaseController.GetCurrentUser(Request);
            //GoogleResultsMonitor model = GoogleResultsMonitor.Get(Id, currentUser.Id);
            GoogleResultsMonitorModel model = googleResultBusiness.GetByOwner(Id, currentUser.Id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }
            
            return View(new RegisterMonitorModel(model.Id, model.Name, model.Query, model.Comments, model.Url, model.Active, model.SendEmail));
        }

        [HttpPost]
        public ActionResult Edit(RegisterMonitorModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var currentUser = BaseController.GetCurrentUser(Request);


            GoogleResultsMonitorModel editModel = new GoogleResultsMonitorModel(model.Id, model.Name, model.Query,
                string.Format(UrlGoogle, Server.HtmlEncode(model.Query)), model.Comments, currentUser.Id, model.Active, model.SendEmail);

            googleResultBusiness.Update(editModel);

            return RedirectToAction("Index");
        }

        public ActionResult Details(int Id)
        {
            var currentUser = BaseController.GetCurrentUser(Request);
            //GoogleResultsMonitor model = GoogleResultsMonitor.Get(Id, currentUser.Id);
            GoogleResultsMonitorModel model = googleResultBusiness.GetByOwner(Id, currentUser.Id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(new RegisterMonitorModel(model.Id, model.Name, model.Query, model.Comments, model.Url, model.Active, model.SendEmail));
        }

        public ActionResult Delete(int Id)
        {
            var currentUser = BaseController.GetCurrentUser(Request);
            //GoogleResultsMonitor model = GoogleResultsMonitor.Get(Id, currentUser.Id);
            GoogleResultsMonitorModel model = googleResultBusiness.GetByOwner(Id, currentUser.Id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            return View(new RegisterMonitorModel(model.Id, model.Name, model.Query, model.Comments, model.Url, model.Active, model.SendEmail));
        }
        
        [HttpPost]
        public ActionResult Delete(int Id, GoogleResultsMonitorModel delete)
        {
            var currentUser = BaseController.GetCurrentUser(Request);
            GoogleResultsMonitorModel model = googleResultBusiness.GetByOwner(Id, currentUser.Id);
            if (model == null)
            {
                return RedirectToAction("Index");
            }

            googleResultBusiness.Delete(model.Id);
            return RedirectToAction("Index");
        }


        public ActionResult GetMonitorTransactions(FilterMonitorModel model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(new List<GoogleMonitorTransactionModel>());
            }

            List<GoogleMonitorTransactionModel> transactions = new List<GoogleMonitorTransactionModel>();
            if (model.StartDate == null && model.EndDate == null)
            {
                var currentUser = BaseController.GetCurrentUser(Request);
                //transactions = GoogleMonitorTransaction.Get(model.IdMonitor);
                transactions = googleTransactionBusiness.GetList(model.IdMonitor);
                return PartialView(transactions);
            }

            //transactions = GoogleMonitorTransaction.Get(model.IdMonitor, model.StartDate, model.EndDate);
            transactions = googleTransactionBusiness.GetList(model.IdMonitor, model.StartDate, model.EndDate);

            return PartialView(transactions);
        }

        public ActionResult GetCSVTransactions(FilterMonitorModel model)
        {

            var currentUser = BaseController.GetCurrentUser(Request);
            GoogleResultsMonitorModel monitor = googleResultBusiness.GetByOwner(model.IdMonitor, currentUser.Id);

            List<GoogleMonitorTransactionModel> transactions = new List<GoogleMonitorTransactionModel>();
            if (model.StartDate == null && model.EndDate == null)
            {
                transactions = googleTransactionBusiness.GetList(model.IdMonitor);
            }
            else
            {
                transactions = googleTransactionBusiness.GetList(model.IdMonitor, model.StartDate, model.EndDate);
            }


            var filename = Server.MapPath("/TmpFiles/" + Guid.NewGuid().ToString().Replace("-", "") + ".csv");
            textWriter = System.IO.File.CreateText(filename);
            var csv = new CsvWriter(textWriter);


            csv.WriteField("Date");
            csv.WriteField("FormattedResult");
            csv.WriteField("Query");
            csv.WriteField("Url");
            csv.WriteField("NumberOfResults");
            csv.WriteField("TimeSearch");
            csv.NextRecord();
            foreach (var item in transactions)
            {
                csv.WriteField(item.Date.ToString());
                csv.WriteField(item.FormattedResult);
                csv.WriteField(item.Query);
                csv.WriteField(item.Url);
                csv.WriteField(item.NumberOfResults);
                csv.WriteField(item.TimeSearch);
                csv.NextRecord();
            }
            
            //csv.WriteRecords(transactions);

            textWriter.Close();
            byte[] bytes = System.IO.File.ReadAllBytes(filename);
            System.IO.File.Delete(filename);
            return File(bytes, "text/csv", monitor.Name + "_" + DateTime.Now.ToString() + ".csv");

        }
    }
}