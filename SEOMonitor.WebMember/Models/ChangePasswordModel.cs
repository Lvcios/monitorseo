﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SEOMonitor.WebMember.Models
{
    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña actual")]
        public string CurrentPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Nueva contraseña")]
        [MaxLength(10, ErrorMessage = "La contraseña debe tener como máximo 10 carácteres")]
        [MinLength(6, ErrorMessage = "La contraseña debe tener como mínimo 6 carácteres")]
        public string NewPassword { get; set; }

        
        [DataType(DataType.Password)]
        [Compare("NewPassword")]
        [Display(Name ="Confirme nueva contreña")]
        public string ConfirmNewPassword { get; set; }
        
    }
}