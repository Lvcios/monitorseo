﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SEOMonitor.WebMember.Models
{
    public class FilterMonitorModel
    {

        public int IdMonitor { get; set; }

        [Display(Name = "Fecha inicial")]
        [DataType(DataType.Date)]
        public DateTime? StartDate { get; set; }

        [Display(Name = "Fecha Final")]
        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }
    }
}