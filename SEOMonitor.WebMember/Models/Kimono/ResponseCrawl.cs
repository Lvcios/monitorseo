﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SEOMonitor.WebMember.Models.Kimono
{
    public class ResponseCrawl
    {
        public string name { get; set; }
        public int count { get; set; }
        public string frequency { get; set; }
        public int version { get; set; }
        public bool newdata { get; set; }
        public string lastrunstatus { get; set; }
        public string thisversionstatus { get; set; }
        public string thisversionrun { get; set; }
        public dynamic results { get; set; }
    }
}