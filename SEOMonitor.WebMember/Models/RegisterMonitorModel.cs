﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SEOMonitor.WebMember.Models
{
    public class RegisterMonitorModel
    {

        public long Id { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        [MaxLength(50)]
        [MinLength(3)]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Display(Name = "Consulta")]
        [Required]
        [DataType(DataType.Text)]
        [MaxLength(50)]
        [MinLength(3)]
        public string Query { get; set; }
        
        [Display(Name = "Comentarios")]
        [MaxLength(140)]
        [MinLength(10)]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }
        
        public string Url { get; set; }

        [Display(Name = "Activo")]
        public bool Active { get; set; }

        [Display(Name = "Activar envío de emails")]
        public bool SendEmail { get; set; }

        public RegisterMonitorModel() { }


        public RegisterMonitorModel(long id, string name, string query, string comments, string url, bool active, bool sendmail)
        {
            this.Id = id;
            this.Name = name;
            this.Query = query;
            this.Comments = comments;
            this.Url = url;
            this.Active = active;
            this.SendEmail = sendmail;
        }
    }
}