﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace SEOMonitor.WebMember.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/content/smartadmin").Include(
                "~/Content/SmartAdmin/css/*.min.css"));

            bundles.Add(new ScriptBundle("~/scripts/smartadmin").Include(
                "~/Scripts/SmartAdmin/app.config.js",
                "~/Scripts/SmartAdmin/plugin/jquery-touch/jquery.ui.touch-punch.min.js",
                "~/Scripts/SmartAdmin/bootstrap/bootstrap.min.js",
                "~/Scripts/SmartAdmin/notification/SmartNotification.min.js",
                "~/Scripts/SmartAdmin/smartwidgets/jarvis.widget.min.js",
                "~/Scripts/SmartAdmin/plugin/jquery-validate/jquery.validate.min.js",
                "~/Scripts/SmartAdmin/plugin/masked-input/jquery.maskedinput.min.js",
                "~/Scripts/SmartAdmin/plugin/select2-4.0.0/js/select2.min.js",
                "~/Scripts/SmartAdmin/plugin/bootstrap-slider/bootstrap-slider.min.js",
                "~/Scripts/SmartAdmin/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js",
                "~/Scripts/SmartAdmin/plugin/msie-fix/jquery.mb.browser.min.js",
                "~/Scripts/SmartAdmin/plugin/fastclick/fastclick.min.js",
                "~/Scripts/SmartAdmin/app.min.js"));

            bundles.Add(new ScriptBundle("~/scripts/full-calendar").Include(
                "~/Scripts/SmartAdmin/plugin/moment/moment.min.js",
                "~/Scripts/SmartAdmin/plugin/fullcalendar/jquery.fullcalendar.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/charts").Include(
                "~/Scripts/SmartAdmin/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js",
                "~/Scripts/SmartAdmin/plugin/sparkline/jquery.sparkline.min.js",
                "~/Scripts/SmartAdmin/plugin/morris/morris.min.js",
                "~/Scripts/SmartAdmin/plugin/morris/raphael.min.js",
                "~/Scripts/SmartAdmin/plugin/flot/jquery.flot.cust.min.js",
                "~/Scripts/SmartAdmin/plugin/flot/jquery.flot.resize.min.js",
                "~/Scripts/SmartAdmin/plugin/flot/jquery.flot.time.min.js",
                "~/Scripts/SmartAdmin/plugin/flot/jquery.flot.fillbetween.min.js",
                "~/Scripts/SmartAdmin/plugin/flot/jquery.flot.orderBar.min.js",
                "~/Scripts/SmartAdmin/plugin/flot/jquery.flot.pie.min.js",
                "~/Scripts/SmartAdmin/plugin/flot/jquery.flot.tooltip.min.js",
                "~/Scripts/SmartAdmin/plugin/dygraphs/dygraph-combined.min.js",
                "~/Scripts/SmartAdmin/plugin/chartjs/chart.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/datatables").Include(
                "~/Scripts/SmartAdmin/plugin/datatables/jquery.dataTables.min.js",
                "~/Scripts/SmartAdmin/plugin/datatables/dataTables.colVis.min.js",
                "~/Scripts/SmartAdmin/plugin/datatables/dataTables.tableTools.min.js",
                "~/Scripts/SmartAdmin/plugin/datatables/dataTables.bootstrap.min.js",
                "~/Scripts/SmartAdmin/plugin/datatable-responsive/datatables.responsive.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/jq-grid").Include(
                "~/Scripts/SmartAdmin/plugin/jqgrid/jquery.jqGrid.min.js",
                "~/Scripts/SmartAdmin/plugin/jqgrid/grid.locale-en.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/forms").Include(
                "~/Scripts/SmartAdmin/plugin/jquery-form/jquery-form.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/smart-chat").Include(
                "~/Scripts/SmartAdmin/smart-chat-ui/smart.chat.ui.min.js",
                "~/Scripts/SmartAdmin/smart-chat-ui/smart.chat.manager.min.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/vector-map").Include(
                "~/Scripts/SmartAdmin/plugin/vectormap/jquery-jvectormap-1.2.2.min.js",
                "~/Scripts/SmartAdmin/plugin/vectormap/jquery-jvectormap-world-mill-en.js"
                ));
        }
    }
}