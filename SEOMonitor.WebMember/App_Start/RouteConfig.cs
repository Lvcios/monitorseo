﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SEOMonitor.WebMember
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Index", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "Kimono",
            //    url: "{controller}/{action}/{id_monitor}",
            //    defaults: new { controller = "Kimono", action = "Index", id_monitor = UrlParameter.Optional }
            //);
        }
    }
}
